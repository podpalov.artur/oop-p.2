package Train;

import java.util.Comparator;

public class TrainComparator2 implements Comparator<Train> {
    @Override
    public int compare(Train o1, Train o2) {
        int n1 = o1.getTrainNumber();
        int n2 = o2.getTrainNumber();
        return Integer.compare(n1, n2);
    }
}
