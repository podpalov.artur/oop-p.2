package Train;

public class TrainMain {
    public static void main(String[] args) {
        Train t1 = new Train("Cochabamba", 4327, "22:40");
        Train t2 = new Train("Llanfairpwllgwyngyll", 5432, "08:14");
        Train t3 = new Train("Cochabamba", 74423, "05:28");
        Train t4 = new Train("Enugu", 2280, "16:46");
        Train t5 = new Train("Bishkek", 7361, "00:22");
        Train[] trains = {t1, t2, t3, t4, t5};
        TrainArray trainArray = new TrainArray(trains);
        trainArray.sortByDestination();
        trainArray.print();
        trainArray.sortByNumber();
        trainArray.print();
        trainArray.displayTrainInfo(2280);
    }
}


