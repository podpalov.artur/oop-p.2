package Train;

import java.util.Arrays;

public class TrainArray {
    Train[] trains;

    public TrainArray(Train[] trains) {
        this.trains = trains;
    }

    public Train[] getTrains() {
        return trains;
    }

    public void setTrains(Train[] trains) {
        this.trains = trains;
    }

    public void sortByDestination() {
        System.out.println("Trains sorted by destination");
        Arrays.sort(trains, new TrainComparator());
    }

    public void sortByNumber() {
        System.out.println("Trains sorted by number");
        Arrays.sort(trains, new TrainComparator2());
    }

    public void print() {
        for (Train train : trains) {
            System.out.println(train.toString());
        }
    }

    public void displayTrainInfo(int trainNumber) {
        boolean trainNumberExist = false;
        for (Train train : trains) {

            if (train.getTrainNumber() == trainNumber) {
                System.out.println("Train number: " + train.getTrainNumber());
                System.out.println("Destination: " + train.getDestinationName());
                System.out.println("Departure time: " + train.getDepartureTime());
                trainNumberExist = true;
            }

        }
        if (!trainNumberExist) {
            System.out.println("There is no train with number " + trainNumber);
        }
    }
}

