package Train;

import java.util.Comparator;
import java.util.Objects;

public class TrainComparator implements Comparator<Train> {
    @Override
    public int compare(Train o1, Train o2) {
        String destination1 = o1.getDestinationName();
        String destination2 = o2.getDestinationName();
        String time1 = o1.getDepartureTime();
        String time2 = o2.getDepartureTime();
        if (Objects.equals(destination1, destination2)) {
            return time1.compareTo(time2);
        } else {
            return destination1.compareTo(destination2);
        }
    }
}

