package Book;

class BookMain {
    public static void main(String[] args) {
        Book b1 = new Book(1, "The Lord of the Rings", "John Ronald Reuel Tolkien", "Allen & Unwin", 1954, 1178, "24,99$", "Hardcover");
        Book b2 = new Book(2, "Harry Potter and the Prisoner of Azkaban", "Joanne Rowling", "Bloomsbury", 1999, 317, "20,49$", "Paperback");
        Book b3 = new Book(3, "Time of Contempt", "Andrzej Sapkowski", "superNOWA", 1995, 352, "24,99", "Hardcover");
        Book[] books = {b1, b2, b3};
        BookArray booksArray = new BookArray(books);
        booksArray.booksByAuthor("Andrzej Sapkowski");
        booksArray.booksByPublisher("Allen & Unwin");
        booksArray.booksByYear(1999);
    }
}
