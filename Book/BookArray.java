package Book;

import java.util.Objects;

class BookArray {
    Book[] books;

    public BookArray(Book[] books) {
        this.books = books;
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public void booksByAuthor(String author) {
        boolean bookExist = false;
        for (Book book : books) {
            if (Objects.equals(book.getAuthor(), author)) {
                System.out.println(book);
                bookExist = true;
            }
        }
        if (!bookExist) {
            System.out.println("There is no book by this author");
        }
    }

    public void booksByPublisher(String publisher) {
        boolean bookExist = false;
        for (Book book : books) {
            if (Objects.equals(book.getPublisher(), publisher)) {
                System.out.println(book);
                bookExist = true;
            }
        }
        if (!bookExist) {
            System.out.println("There is no book by this publisher");
        }
    }

    public void booksByYear(int year) {
        boolean bookExist = false;
        for (Book book : books) {
            if (book.getYear() == year) {
                System.out.println(book);
                bookExist = true;
            }
        }
        if (!bookExist) {
            System.out.println("There is no book that was published this year");
        }
    }
}

