package Airline;

import java.util.Objects;

class AirlineArray {
    Airline[] airlines;

    public AirlineArray(Airline[] airlines) {
        this.airlines = airlines;
    }

    public Airline[] getAirlines() {
        return airlines;
    }

    public void setAirlines(Airline[] airlines) {
        this.airlines = airlines;
    }

    public void airlinesByDestination(String destination) {
        boolean flyExist = false;
        for (Airline airline : airlines) {
            if (Objects.equals(airline.getDestination(), destination)) {
                System.out.println(airline);
                flyExist = true;
            }
        }
        if (!flyExist) {
            System.out.println("There is no airlines for this destination");
        }
    }

    public void airlinesByDay(String day) {
        boolean flyExist = false;
        for (Airline airline : airlines) {
            for (String dayOfWeek : airline.getDaysOfTheWeek()) {
                if (dayOfWeek.equals(day)) {
                    System.out.println(airline);
                    flyExist = true;
                }
            }
        }
        if (!flyExist) {
            System.out.println("There is no airlines for this day");
        }
    }

    public void airlinesByDayAndTime(String day, String time) {
        boolean flyExist = false;
        for (Airline airline : airlines) {
            if (airline.getDepartureTime().compareTo(time) > 0) {
                for (String dayOfWeek : airline.getDaysOfTheWeek()) {
                    if (dayOfWeek.equals(day)) {
                        System.out.println(airline);
                        flyExist = true;
                    }
                }
            }
        }
        if (!flyExist) {
            System.out.println("There is no airlines for this day and time");
        }
    }
}

