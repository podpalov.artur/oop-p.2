package Airline;

class AirlineMain {
    public static void main(String[] args) {
        Airline a1 = new Airline("Hamburg", 31791, "Boeing 767", "08:14", new String[]{"Tuesday", "Saturday"});
        Airline a2 = new Airline("Johannesburg", 9734, "Boeing 757", "10:03", new String[]{"Monday", "Wednesday", "Sunday"});
        Airline a3 = new Airline("Simferopol", 2126, "An-225 Mriya", "09:20", new String[]{"Thursday", "Friday", "Saturday"});
        Airline a4 = new Airline("Reykjavik", 2280, "Boeing 747-200B", "22:01", new String[]{"Wednesday", "Thursday", "Sunday"});
        Airline[] airlines = {a1, a2, a3, a4};
        AirlineArray airlinesArray = new AirlineArray(airlines);
        airlinesArray.airlinesByDay("Wednesday");
        airlinesArray.airlinesByDestination("Hamburg");
        airlinesArray.airlinesByDayAndTime("Saturday", "00:00");
    }
}

