package Customer;

public class CustomerMain {
    public static void main(String[] args) {
        Customer c1 = new Customer(31412, 4214, 1, "Ibrahim", "Mustapha", "Muhammed", "Omar st.");
        Customer c2 = new Customer(14246, 8762, 2, "Sin", "Pin", "Zhang", "Winni Puh st.");
        Customer c3 = new Customer(98162, 9375, 3, "Juan", "Martinez", "Santiago", "Rodrigo st.");
        Customer[] customers = {c1, c2, c3};
        CustomerArray customersList = new CustomerArray(customers);
        customersList.sortCustomersByName();
        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
        int cardIntervalBegin = 0;
        int cardIntervalEnd = 4215;
        customersList.customersInInterval(cardIntervalBegin, cardIntervalEnd);
    }
}
