package Customer;

import java.util.Comparator;
import java.util.Objects;

public class CustomerComparator implements Comparator<Customer> {
    @Override
    public int compare(Customer o1, Customer o2) {
        String name1 = o1.getFirstName();
        String surname1 = o1.getSurname();
        String patronymic1 = o1.getPatronymic();
        String name2 = o2.getFirstName();
        String surname2 = o2.getSurname();
        String patronymic2 = o2.getPatronymic();
        if (Objects.equals(name1, name2)) {
            if (Objects.equals(surname1, surname2)) {
                return patronymic1.compareTo(patronymic2);
            } else {
                return surname1.compareTo(surname2);
            }
        } else {
            return name1.compareTo(name2);
        }
    }
}

