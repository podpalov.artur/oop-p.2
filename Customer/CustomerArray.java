package Customer;

import java.util.Arrays;


class CustomerArray {
    Customer[] customers;

    public CustomerArray(Customer[] customers) {
        this.customers = customers;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public void sortCustomersByName() {
        Arrays.sort(customers, new CustomerComparator());
    }

    public void customersInInterval(int begin, int end) {
        boolean cardExist = false;
        for (Customer customer : customers) {
            if (customer.getCreditCardNumber() >= begin && customer.getCreditCardNumber() <= end) {
                System.out.println(customer);
                cardExist = true;
            }
        }
        if (!cardExist) {
            System.out.println("There is no customer with card in this range");
        }
    }
}


