package Student;

public class StudentMain {
    public static void main(String[] args) {
        Student[] students = new Student[10];
        students[0] = new Student("Mercury", "F.", 1, new int[]{5, 7, 2, 9, 5});
        students[1] = new Student("Jordan", "M.", 2, new int[]{1, 6, 9, 4, 7});
        students[2] = new Student("Presley", "E.", 3, new int[]{10, 2, 8, 5, 1});
        students[3] = new Student("Jobs", "S.", 4, new int[]{2, 1, 5, 10, 8});
        students[4] = new Student("Obama", "B.", 5, new int[]{7, 5, 2, 9, 4});
        students[5] = new Student("Churchill", "W", 6, new int[]{1, 5, 10, 8, 3});
        students[6] = new Student("Walter", "W.", 7, new int[]{9, 10, 9, 10, 10});
        students[7] = new Student("Lincoln", "A.", 8, new int[]{7, 6, 3, 10, 5});
        students[8] = new Student("Lennon", "J", 9, new int[]{10, 7, 4, 2, 8});
        students[9] = new Student("Biden", "J.", 10, new int[]{6, 8, 1, 2, 10});

        int counter = 0;
        for (Student student : students) {
            int[] academicPerformance = student.getAcademicPerformance();
            for (int j = 0; j < academicPerformance.length; j++) {
                counter = j + 1;
                if (academicPerformance[j] < 9) {
                    break;
                }
            }
            if (counter == 5) {
                System.out.println(student.getSurname() + " " + student.getInitials() + " #" + student.getGroupNumber());
                counter = 0;
            }
        }
    }
}

